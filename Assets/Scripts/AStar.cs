using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections;
using System.Threading;

public class AStar : MonoBehaviour
{
    private List<Vector2Int> path;
    private bool pathFilled = true;
    private float waitBetweenColoringDuration = 0.1f;

    public List<Vector2Int> GetPath()
    {
        return path;
    }
    public bool IsPathFilled()
    {
        return pathFilled;
    }
    public void ComputePath(Vector2Int source, Vector2Int goal, List<List<MazeTileType>> maze, Heuristic heuristic, Maze parentMaze)
    {
        path = new List<Vector2Int>();
        pathFilled = false;
        StartCoroutine(ComputePathCo(source, goal, maze, heuristic, parentMaze));
    }

    private IEnumerator ComputePathCo(Vector2Int source, Vector2Int goal, List<List<MazeTileType>> maze, Heuristic heuristic, Maze parentMaze)
    {
        if (maze[goal.y][goal.x] == MazeTileType.Wall)
        {
            path = null;
            pathFilled = true;
            yield break;
        }

        List<List<Tile>> tiles = new List<List<Tile>>();
        List<Tile> opened = new List<Tile>();
        List<Vector2Int> closed = new List<Vector2Int>();

        for ( int i = 0; i < maze.Count; i++)
        {
            tiles.Add(new List<Tile>());
            for (int j = 0; j < maze[i].Count; j++)
            {
                Vector2Int index = new Vector2Int(j, i);
                tiles[i].Add(new Tile(index, int.MaxValue, heuristic == Heuristic.Euclidian ? getEuclidianCost(index, goal) : 0));
            }
        }

        Tile v = tiles[source.y][source.x];
        Tile goalTile = tiles[goal.y][goal.x];
        v.setG(0);
        v.setH(0);

        while (v != goalTile)
        {
            closed.Add(v.getIndex());
            parentMaze.SetFreeTileColor(v.getIndex(), Color.red);
            yield return new WaitForSeconds(waitBetweenColoringDuration);

            List<Vector2Int> neighbours = new List<Vector2Int>();
            for(int i = -1; i <= 1; i++)
                for(int j = -1; j <= 1; j++)
                {
                    Vector2Int index = new Vector2Int(i, j) + v.getIndex();
                    if (!(i == 0 && j == 0) && 
                        GameManager.Instance.Maze.IsValidTileOfType(index, MazeTileType.Free) &&
                        GameManager.Instance.Maze.IsPathFromTiles(v.getIndex(), index) &&
                        !closed.Contains(index))
                        neighbours.Add(index);
                }

            foreach (Vector2Int n in neighbours)
            {
                Tile u = tiles[n.y][n.x];
                int gNew = v.getG() + getEuclidianCost(v.getIndex(), u.getIndex());
                if(gNew < u.getG())
                {
                    u.setG(gNew);
                    opened.Add(u);
                    u.SetParent(v);

                    parentMaze.SetFreeTileColor(u.getIndex(), Color.green);
                    yield return new WaitForSeconds(waitBetweenColoringDuration);
                }
            }

            int smallest = int.MaxValue; 
            int smallestH = int.MaxValue;
            foreach (Tile t in opened)
            {
                if (t.getF() < smallest)
                {
                    smallest = t.getF();
                    smallestH = t.getH();
                    v = t;
                }
                else if (t.getF() == smallestH)
                {
                    if(t.getH() < smallestH)
                    {
                        smallest = t.getF();
                        smallestH = t.getH();
                        v = t;
                    }
                }
            }
            opened.Remove(v);
        }

        List<Vector2Int> newPath = new List<Vector2Int>();
        Tile sourceTile = tiles[source.y][source.x];
        v = goalTile;
        newPath.Add(v.getIndex());
        parentMaze.SetFreeTileColor(v.getIndex(), Color.blue);

        while (v != sourceTile)
        {
            newPath.Insert(0, v.getParent().getIndex());
            v = v.getParent();
            parentMaze.SetFreeTileColor(v.getIndex(), Color.blue);
        }

        path = newPath;
        pathFilled = true;
    }

    private static int getEuclidianCost(Vector2Int a, Vector2Int b)
    {
        return (int)((a - b).magnitude * 10);
    }
    
    public enum Heuristic
    {
        Euclidian,
        Null
    }
}