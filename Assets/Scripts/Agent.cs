﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Principal;
using UnityEngine;

public class Agent : MonoBehaviour
{
    public AStar astar;
    private GameManager gameManager;
    public Vector2Int CurrentTile { get; private set; }

    private Sprite _sprite;
    public Sprite Sprite
    {
        get
        {
            if(_sprite == null)
            {
                _sprite = GetComponentInChildren<SpriteRenderer>()?.sprite;
            }

            return _sprite;
        }
    }

    protected float movementSpeed; // in "tile" units
    protected Maze parentMaze;
    protected bool isInitialized = false;

    private bool isWalking;
    private float waitBeforeResetDuration = 1.5f;

    private List<IEnumerator> walkRoutines;

    protected virtual void Start()
    {
        gameManager = GameManager.Instance;
        gameManager.DestinationChanged += OnDestinationChanged;
        walkRoutines = new List<IEnumerator>();
    }

    protected virtual void Update()
    {
        // TODO Assignment 2 ... this function might be of your interest. :-)
        // You are free to add new functions, create new classes, etc.
        // ---
        // The CurrentTile property should hold the current location (tile-based) of an agent
        //
        // Have a look at Maze class, it contains several useful properties and functions.
        // For example, Maze.MazeTiles stores the information about the tiles of the maze.
        // Then, there are several functions for conversion/retrieval of tile positions, as well as for changing tile colors.
        // 
        // Finally, you can also have a look at GameManager to see what it provides.

        // NOTE
        // The code below is just a simple demonstration of some of the functionality / functions
        // You will need to replace it / change it

        var oldTile = CurrentTile;
        var afterTranslTile = parentMaze.GetMazeTileForWorldPosition(transform.position);

        if (oldTile != afterTranslTile)
            CurrentTile = afterTranslTile;
    }

    public bool CanAgentGo()
    {
        return astar.IsPathFilled();
    }

    // This function is called every time the user sets a new destination using a left mouse button
    protected virtual void OnDestinationChanged(Vector2Int newDestinationTile)
    {
        // TODO Assignment 2 ... this function might be of your interest. :-)
        // The destination tile index is also accessible via GameManager.Instance.DestinationTile

        if (!CanAgentGo())
        {
            Debug.Log("A");
            return;
        }

        StopWalking();
        parentMaze.ResetTileColors();

        astar.ComputePath(CurrentTile, gameManager.DestinationTile, gameManager.Maze.MazeTiles, gameManager.Heuristic, parentMaze);

        gameManager.utility.DoActionAfterWaitBool(() => astar.IsPathFilled(), delegate
        {
            List<Vector2Int> path = astar.GetPath();

            FollowAPath(path, delegate
            {
                Debug.Log("reset");
                if(CanAgentGo())
                    parentMaze.ResetTileColors();
            }, waitBeforeResetDuration);
        });
        
    }

    private void FollowAPath(List<Vector2Int> path, System.Action afterAction, float startAfterDuration)
    {
        isWalking = true;
        afterAction += () => isWalking = false;
        Move(gameManager.Maze.GetWorldPositionForMazeTile(path[0]), () => FollowAPathCo(path, afterAction, startAfterDuration));
    }
    private void FollowAPathCo(List<Vector2Int> path, System.Action afterAction, float startAfterDuration)
    {
        if (!isWalking)
            return;

        if (path.Count > 1)
        {
            path.RemoveAt(0);
            Move(gameManager.Maze.GetWorldPositionForMazeTile(path[0]), () => FollowAPathCo(path, afterAction, startAfterDuration));
        }
        else
            gameManager.utility.DoActionAfter(afterAction, startAfterDuration);
    }
    private void Move(Vector2 destinaton, System.Action afterAction)
    {
        IEnumerator enumerator = MoveCo(destinaton, afterAction);
        walkRoutines.Add(enumerator);
        StartCoroutine(enumerator);
    }
    private IEnumerator MoveCo(Vector2 destinaton, System.Action afterAction)
    {
        float elapsedTime = 0f;
        Vector2 oldPosition = transform.position;
        float duration = (oldPosition - destinaton).magnitude / movementSpeed;

        while(elapsedTime < duration)
        {
            transform.position = Vector2.Lerp(oldPosition, destinaton, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        if (afterAction != null) afterAction();
    }
    
    private void StopWalking()
    {
        isWalking = false;
        foreach (IEnumerator n in walkRoutines)
            StopCoroutine(n);
    }
    public virtual void InitializeData(Maze parentMaze, float movementSpeed, Vector2Int spawnTilePos)
    {
        this.parentMaze = parentMaze;

        // The multiplication below ensures that movement speed is considered in tile-units so it stays
        // consistent across different scales of the maze
        this.movementSpeed = movementSpeed * parentMaze.GetElementsScale().x; 

        transform.position = parentMaze.GetWorldPositionForMazeTile(spawnTilePos.x, spawnTilePos.y);
        transform.localScale = parentMaze.GetElementsScale();

        CurrentTile = spawnTilePos;

        isInitialized = true;
    }
}
