using UnityEngine;

public class Tile
{
    private Vector2Int index;
    private int g;
    private int h;
    private Tile parent;
    public Tile(Vector2Int index, int g, int h)
    {
        this.index = index;
        this.g = g;
        this.h = h;
        parent = null;
    }
    public Vector2Int getIndex() { return index; }
    public int getG() { return g; }
    public int getH() { return h; }
    public void setG(int g) { this.g = g; }

    public void setH(int h) {  this.h = h; }
    public int getF() { return g + h; }
    public Tile getParent() { return parent; }
    public void SetParent(Tile parent) { this.parent = parent; }
}