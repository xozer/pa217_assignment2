using System.Collections;
using UnityEngine;
using System;

public class Utility : MonoBehaviour 
{
    /// <summary>
    /// Waits for duration amount and does the action
    /// </summary>
    public void DoActionAfter(Action action, float duration)
    {
        StartCoroutine(DoActionAfterCo(action, duration));
    }
    private IEnumerator DoActionAfterCo(Action action, float duration)
    {
        yield return new WaitForSeconds(duration);
        action();
    }
    public void DoActionAfterWaitBool(Func<bool> boolean, Action action)
    {
        StartCoroutine(DoActionAfterWaitBoolCo(boolean, action));
    }
    private IEnumerator DoActionAfterWaitBoolCo(Func<bool> boolean, Action action)
    {
        yield return new WaitUntil(boolean);
        action();
    }
}